﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MuhendislikFirmasi.Startup))]
namespace MuhendislikFirmasi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
